# Morello CI Trigger

This project runs a scheduled pipeline that polls "trigger-pipeline" label along with label with a prefix "group-" on any MR and triggers the pipeline on manifest project with details of all the MRs that have same label "group-*"

Documentation is consolidated in the [Morello CI Documentation project](https://git.morello-project.org/morello/morello-ci-documentation).
